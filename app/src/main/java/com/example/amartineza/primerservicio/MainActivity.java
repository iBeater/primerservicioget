package com.example.amartineza.primerservicio;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String CITIES_ENDPOINT = "http://api.cinehoyts.cl/v1/cities";
    public static final String CITIES_CINEMAS = "http://www.cinepolis.com/manejadores/CiudadesComplejos.ashx?EsVIP=false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        callService();
    }

    private  void  callService(){
        MyAsynTask myAsynTask = new MyAsynTask();
        myAsynTask.execute(CITIES_ENDPOINT);
        // myAsynTask.execute(CITIES_CINEMAS);
    }
}

class MyAsynTask extends AsyncTask<String, Integer, Double>{
    public MyAsynTask() {
        super();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Double aDouble) {
        super.onPostExecute(aDouble);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Double aDouble) {
        super.onCancelled(aDouble);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected Double doInBackground(String... strings) {
        consumeGetService(strings[0]); //Primer Get
     /*   String valueToSend = strings[0];
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPOST = new HttpPost(valueToSend);

        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("myHttpData", valueToSend));

        try {
            HttpResponse response = client.execute(httpPOST);
            Log.e("Tag","");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }*/

        return null;
    }

    private void consumeGetService(String valueToSend){
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(valueToSend);

        List<NameValuePair> valuePairs = new ArrayList<>();
        valuePairs.add(new BasicNameValuePair("myHttpData", valueToSend));

        try {
            HttpResponse response = client.execute(httpGet);
            String json = EntityUtils.toString(response.getEntity());

            Gson gson = new Gson();
            Type listType = new TypeToken<List<CitiesModel>>(){}.getType();
            List<CitiesModel> cities = (List<CitiesModel>) gson.fromJson(json, listType);
            for(CitiesModel city: cities) {
               String estado= city.getEstado();
                Log.e("Estado", estado);
            }

            Log.e("Tag", "");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

}
